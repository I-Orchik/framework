#-------------------------------------------------
#
# Project created by QtCreator 2014-10-10T14:31:52
#
#-------------------------------------------------

QT       += core
QT       += network

QT       -= gui

TARGET = Framework
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    framework.cpp

HEADERS += \
    framework.h
