#include "framework.h"


Framework::Framework(const QUrl &baseUrl, QObject *parent)
{
    qDebug() << baseUrl.toDisplayString();
    this->baseUrl = baseUrl;
    manager = new QNetworkAccessManager(this);

    qDebug() << this->baseUrl.toDisplayString();

}

void Framework::createDatabase(const QString &dbName)       //метод создания БД
{
    QByteArray clearArray;
    //dbName.append("/")
    qDebug() << "Before setPath" << this->baseUrl.toDisplayString();
    qDebug() << "Path" << dbName << endl;
    this->baseUrl.setPath("/" + dbName);
    qDebug() << this->baseUrl.toDisplayString();
    QNetworkRequest requestCouchDb = QNetworkRequest(baseUrl);                      //создаем запрос
    QNetworkReply *reply = manager->put(requestCouchDb,clearArray);                 //ответ

    bool connected = connect(reply, SIGNAL(finished()), this, SLOT(finished()));    //
    Q_ASSERT(connected);
    qDebug() << baseUrl.toString();
}

void Framework::createDocumentInDatabase(const QString &documentName, const QString &databaseName) //метод создания документа
{

}


void Framework::setContinuousReplication(const QString &fromDatabaseName, const QString &toDatabaseName) //метод репликации в кнтинуусе
{

}

void Framework::finished()
{
    QNetworkReply *reply = (QNetworkReply *)sender();
    reply->error();
    qDebug() << reply;
}
