#ifndef FRAMEWORK_H
#define FRAMEWORK_H

#include <QObject>
#include <QTextStream>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QString>

class Framework : public QObject
{
    Q_OBJECT
public:
    explicit Framework(const QUrl &baseUrl, QObject *parent = 0);

signals:

public slots:
    void createDatabase(const QString &databaseName);
    void createDocumentInDatabase(const QString &documentName, const QString &databaseName);
    void setContinuousReplication(const QString &fromDatabaseName, const QString &toDatabaseName);
private slots:
    void finished();

private:
    QNetworkAccessManager *manager;
    QUrl baseUrl;
};

#endif // FRAMEWORK_H
